# Example setup with NextJS, TailwindCSS and Next-intl, with I18nexus translations
- Export api key for I18nexus project or provide language files in `src/locales` folder
## Let's start
- Create a new project with I18nexus and put the api key in `.env` file like `I18NEXUS_API_KEY="your_api_key_here"`
- Generate AuthJS token with `npx auth secret` (or generate another secret key) and put it in `.env` file like this: `AUTH_SECRET="secret"`