import createMiddleware from 'next-intl/middleware';
import { pathnames, locales, localePrefix, defaultLocale } from "./config";
import { NextRequest, NextResponse } from 'next/server'
import { auth } from './auth';

export default async function middleware(request: NextRequest & { auth?: any }): Promise<NextResponse> {
  // remove first item (lang) from path by taking only string after second slash
  const actualPath = request.nextUrl.pathname.split('/').slice(2).join('/');
  // check if the request is the secured app path
  if (actualPath.startsWith('app')) {
    // first check user session with auth middleware
    const session = await auth();

    if (!session) {
      const url = request.nextUrl.clone();
      url.pathname = '/api/auth/signin'
      return NextResponse.redirect(url)
    }
  }

  // create middleware for localization
  return createMiddleware({
    defaultLocale,
    locales,
    pathnames,
    localePrefix,
  })(request);
}

export const config = {
  // Skip paths starting with /api, /_next, and any path with a dot in it, otherwise we want to match all paths for translation
  matcher: ["/((?!api|_next|.*\\..*).*)"],
};