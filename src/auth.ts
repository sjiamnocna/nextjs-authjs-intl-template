import NextAuth, { NextAuthConfig } from "next-auth";
import github from "next-auth/providers/github";
 
export const authConfig: NextAuthConfig = {
  debug: process.env.NODE_ENV === "development",
  providers: [github],
};

export const { handlers, signIn, signOut, auth } = NextAuth(authConfig);