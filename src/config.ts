import {Pathnames} from 'next-intl/navigation';

export const port = process.env.PORT || 3000;
export const host = process.env.VERCEL_URL
  ? `https://${process.env.VERCEL_URL}`
  : `http://localhost:${port}`;

export const defaultLocale = 'en' as const;
export const locales = ['en', 'cs'] as const;

export const pathnames = {
  '/': '/',
  '/product': {
    en: '/product',
    cs: '/produkt',
  },
  '/company': {
    en: '/company',
    cs: "/nase-spolecnost",
  },
  '/about': {
    en: '/about',
    cs: "/o-nas",
  },
  '/blog': {
    en: '/blog',
    cs: '/blog',
  },
  '/contact': {
    en: '/contact',
    cs: '/kontakt',
  },
} satisfies Pathnames<typeof locales>;

// Use the default: `always`
export const localePrefix = undefined;

export type AppPathnames = keyof typeof pathnames;
