'use client';

import React, { useEffect, useState } from 'react';

import Link from 'next/link';
import ThemeChanger from './DarkSwitch';
import { Disclosure } from '@headlessui/react';
import NavItem, { NavItem_props } from "./navbar_item.comp";
import LanguageSwitcher from './languageSwitcher/switcher';
import { Button } from './ui/button';
import { FaPlayCircle } from 'react-icons/fa';
import Container from './container';
import { useTheme } from 'next-themes';

export type NavItemWithChildren = NavItem_props & {
  children?: NavItem_props[];
};

export type NavbarProps = {
  items: NavItemWithChildren[];
};

const Navbar = ({ items }: NavbarProps): JSX.Element => {
  const {theme} = useTheme();
    
  /**
   * @var background: number - transparent background of the navbar
   */
  const [background, setBackground] = useState<string>('bg-transparent');

  const paintBackground = () => {
    const color = theme === 'dark' ? '25,30,42' : '255,255,255';

    // if this is a mobile or tablet device, do not paint the background
    if (window.innerWidth < 1024) {
      setBackground(`rgb(${color})`);
      return;
    }

    const opacity = (window.scrollY + 50) / 500;
    if (opacity < 0) {
      setBackground('initial');
    } else {
      setBackground(theme === 'dark' ? `rgba(${color},${opacity})` : `rgba(${color},${opacity}`);
    }
  }

  useEffect(() => {
    paintBackground();
  }, [theme]);

  useEffect(() => {
    paintBackground();
    // linearly set opacityparency of the navbar when the page is scrolled down in range from 50 to 100
    window.addEventListener('scroll', paintBackground);

    return () => {
      window.removeEventListener('scroll', paintBackground);
    }
  });

  // fix the menu to the top of the page 
  return (
    <div className="w-full fixed" style={{
      background: background,
    }}>
      <Container className="p-5 xl:px-0">
        <nav className="relative flex flex-wrap items-center justify-between mx-auto lg:justify-between">
          {/* Logo  */}
          <Disclosure>
            {({ open }) => (
              <>
                <div className="flex flex-wrap items-center justify-between w-full lg:w-auto">
                  <Link href='/'>
                    <p className="flex items-center space-x-2 text-2xl font-medium text-indigo-500 dark:text-gray-100">
                      <span>NexTSly</span>
                    </p>
                  </Link>

                  <Disclosure.Button
                    aria-label="Toggle Menu"
                    className="px-2 py-1 ml-auto text-gray-500 rounded-md lg:hidden hover:text-indigo-500 focus:text-indigo-500 focus:bg-indigo-100 focus:outline-none dark:text-gray-300 dark:focus:bg-gray-700"
                  >
                    <svg
                      className="w-6 h-6 fill-current"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                    >
                      {open && (
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M18.278 16.864a1 1 0 0 1-1.414 1.414l-4.829-4.828-4.828 4.828a1 1 0 0 1-1.414-1.414l4.828-4.829-4.828-4.828a1 1 0 0 1 1.414-1.414l4.829 4.828 4.828-4.828a1 1 0 1 1 1.414 1.414l-4.828 4.829 4.828 4.828z"
                        />
                      ) || (
                        <path
                          fillRule="evenodd"
                          d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z"
                        />
                      )}
                    </svg>
                  </Disclosure.Button>

                  <Disclosure.Panel className="flex flex-wrap w-full my-5 lg:hidden">
                    <>
                      {items.map((item, index) => (
                        <Link
                          key={index}
                          href='/'
                          className="w-full px-4 py-2 -ml-4 text-gray-500 rounded-md dark:text-gray-300 hover:text-indigo-500 focus:text-indigo-500 focus:bg-indigo-100 dark:focus:bg-gray-800 focus:outline-none"
                        >
                          {item.title}
                        </Link>
                      ))}
                      <Link
                        href='/'
                        className="w-full px-6 py-2 mt-3 text-center text-white bg-indigo-600 rounded-md lg:ml-5"
                      >
                        Get Started
                      </Link>
                    </>
                  </Disclosure.Panel>
                </div>
              </>
            )}
          </Disclosure>

          {/* menu  */}
          <div className="hidden text-center lg:flex lg:items-center">
            <ul className="items-center justify-end flex-1 pt-6 list-none lg:pt-0 lg:flex">
              {items.map((item, index) => (
                <NavItem {...item} key={index} />
              ))}
              <div className="md:w-10" />
              <li className='mx-2'>
                <ThemeChanger />
              </li>
              <li className='mx-2'>
                <LanguageSwitcher />
              </li>
            </ul>
          </div>

          <div className="hidden mr-3 space-x-4 lg:flex nav__item">
            <Link href='/app'>
              <Button variant='default' className='text-base px-5 py-4'>
                <FaPlayCircle className='mr-2' />
                Get Started
              </Button>
            </Link>
          </div>
        </nav>

      </Container>
    </div>
  );
};

export default Navbar;
