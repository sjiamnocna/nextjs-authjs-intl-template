import { useLocale } from "next-intl";
import Link from "next/link";
import ReactCountryFlag from "react-country-flag";

export type LocaleData = [string, string];

const locales: string[][] = [
    ['en', 'GB', 'English'],
    ['cs', 'CZ', 'Česky'],
];

/**
 * Renders a language represented by a flag linked to the given locale version of the page.
 * @returns 
 */
const LanguageSwitcher = () => {
    const currentLocale = useLocale();

    return (
        <div>
            {locales.map(([locale, flagCode, langTitle]) => {
                if (!locale || !flagCode || !langTitle || locale === currentLocale) {
                    return null;
                }

                return (
                    <Link href={`/${locale}`}>
                        <ReactCountryFlag countryCode={flagCode} svg alt={langTitle} className="block -mt-1 mx-1 p-0 text-xl hover:grayscale" />
                    </Link>
                );
            })}
        </div>
    );
};

export default LanguageSwitcher;