'use client';

import React from 'react';

// @ts-ignore
const Container = (props) => {
  return (
    <div
      className={`container p-4 lg:p-6 mx-auto xl:px-0 ${
        props.className ? props.className : ""
      }`}
    >
      {props.children}
    </div>
  );
};

export default Container;
