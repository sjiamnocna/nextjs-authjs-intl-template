// @ts-nocheck
import React from 'react';

import { Metadata } from 'next';
import { auth } from '@/auth';

export const metadata: Metadata = {
  title: "NexTSly - Free Nextjs & TailwindCSS Landing Page Template",
  description:
    "NexTSly is a free landing page template built with latest next.js (with Typescript) & Tailwind CSS",
};


const Home = async () => {

  // load user session
  const session = await auth();

  return (
    <>
      <div>
        Ahoj, světe!
        {JSON.stringify(session)}
      </div>
    </>
  );
};

export default Home;
