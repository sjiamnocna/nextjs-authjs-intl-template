// Note that `app/[locale]/[...rest]/page.tsx`
// is necessary for this page to render.

export default function NotFoundPage() {
  return (
    <div>
      <p className="max-w-[460px]">Page not found</p>
    </div>
  );
}
