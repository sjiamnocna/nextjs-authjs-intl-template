// @ts-check

const withNextIntl = require("next-intl/plugin")();

module.exports = withNextIntl({
    // TODO: remove eventually if found error with useTranslation and TS
    typescript: {
        ignoreBuildErrors: true,
    },
});
